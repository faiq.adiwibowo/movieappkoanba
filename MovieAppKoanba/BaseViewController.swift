//
//  BaseViewController.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation
import IQKeyboardManagerSwift
import RxSwift

//  MARK:   This class to define global reusable attributes
class BaseViewController: UIViewController {
    var viewWidth = 0.0
    var viewHeight = 0.0
    private let disposeBag = DisposeBag()
    open lazy var keyboardHeight = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        viewWidth = view.frame.width
        viewHeight = view.frame.height
        configureKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: self.view.window)
    }
    
    override func present(_ viewControllerToPresent: UIViewController,
                           animated flag: Bool,
                           completion: (() -> Void)? = nil) {
       viewControllerToPresent.modalPresentationStyle = .fullScreen
       super.present(viewControllerToPresent, animated: flag, completion: completion)
     }
    func presenting(_ viewControllerToPresent: UIViewController,
                            animated flag: Bool,
                            completion: (() -> Void)? = nil) {
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue ?? keyboardSize
                self.view.frame.origin.y -= (offset.height)
            }
        }
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
        }
        self.view.layoutIfNeeded()
    }
   
    // MARK: Animation
    func animateConstraint(constraint: NSLayoutConstraint, constant: CGFloat, duration: TimeInterval? = 0.1){
        UIView.animate(withDuration: duration!) {
            constraint.constant = constant
            self.view.layoutIfNeeded()
        }
    }
    
    func configureKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.resignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.toolbarConfiguration.placeholderConfiguration.showPlaceholder = false
        IQKeyboardManager.shared.toolbarConfiguration.tintColor = .black
    }
    func convertToTime(minutes: Int) -> String {
        let hours = minutes / 60
        let remainingMinutes = minutes % 60
        
        if hours == 0 {
            return "\(remainingMinutes) m"
        } else if remainingMinutes == 0 {
            return "\(hours) h"
        } else {
            return "\(hours) h \(remainingMinutes) m"
        }
    }
    
}
