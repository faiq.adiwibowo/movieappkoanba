//
//  Toast.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 28/04/24.
//

import UIKit

class Toast: UIView {

    var toastType = ToastType.info
    var caption: String? {
        didSet {
            lblCaption.text = caption
        }
    }

    private lazy var lblCaption: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont(.omega, .regular)
        lbl.textColor = .white

        return lbl
    }()

    private lazy var btnClose: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        let btnImage = UIImage(systemName: "xmark")!
        let tintImg = btnImage.withRenderingMode(.alwaysTemplate)
        btn.setImage(tintImg, for: .normal)
        btn.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        btn.tintColor = .white

        return btn
    }()

    var callBackCloseBtn: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
    }

    private func setupView() {

        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: self.frameWidth, height: self.frameHeight))
        contentView.backgroundColor = toastType.color
        contentView.roundCorners(corners: [.topLeft, .topRight], radius: 20)
        contentView.addSubview(lblCaption)
        contentView.addSubview(btnClose)
        lblCaption.translatesAutoresizingMaskIntoConstraints = false
        btnClose.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.trailingAnchor.constraint(equalTo: btnClose.trailingAnchor, constant: 16),
            btnClose.heightAnchor.constraint(equalToConstant: 24),
            btnClose.widthAnchor.constraint(equalToConstant: 24),
            btnClose.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            lblCaption.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            btnClose.leadingAnchor.constraint(equalTo: lblCaption.trailingAnchor, constant: 16),
            lblCaption.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            contentView.bottomAnchor.constraint(equalTo: lblCaption.bottomAnchor, constant: 32)
        ])

        let btnDismiss = UIButton()
        btnDismiss.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        btnDismiss.backgroundColor = UIColor(white: 0, alpha: 0)
        self.addSubview(btnDismiss)
        btnDismiss.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            btnDismiss.topAnchor.constraint(equalTo: self.topAnchor),
            btnDismiss.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: btnDismiss.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: btnDismiss.bottomAnchor),
            contentView.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor, constant: 64),
            self.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }

    @objc
    func didTapClose() {
        callBackCloseBtn?()
    }

}
enum ToastType {
    case info
    case success
    case warning
    case danger

    var color: UIColor {
        switch self {
        case .info:
            return .systemBlue
        case .warning:
            return .systemOrange
        case .danger:
            return .systemRed
        case .success:
            return .systemGreen
        }
    }
}
