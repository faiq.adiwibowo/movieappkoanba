//
//  Router.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    
    case nowPlaying(page: String)
    case search(query: String)
    case details(movieId: String)
    case review(movieId: String)
    case trailer(movieId: String)
    case credits(movieId: String)
    
    var method: HTTPMethod {
        switch self {
        case .nowPlaying:
            return .get
        case .search:
            return .get
        case .details:
            return .get
        case .review:
            return .get
        case .trailer:
            return .get
        case .credits:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .nowPlaying:
            return nil
        case .search:
            return nil
        case .details:
            return nil
        case .review:
            return nil
        case .trailer:
            return nil
        case .credits:
            return nil
        }
    }
    
    var url: URL {
        switch self {
        case .nowPlaying(let page):
            let queryString = Constants.BaseURL.nowPlayingURL + "&page=" + page
            let url = URL(string: queryString)!
            return url
        case .search(let query):
            let queryString = Constants.BaseURL.searchURL + query
            let url = URL(string: queryString)!
            return url
        case .details(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.detailString
            let url = URL(string: queryString)!
            return url
        case .review(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.reviewString
            let url = URL(string: queryString)!
            print(url)
            return url
        case .trailer(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.trailerString
            let url = URL(string: queryString)!
            return url
        case .credits(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.creditsString
            let url = URL(string: queryString)!
            return url
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
}
