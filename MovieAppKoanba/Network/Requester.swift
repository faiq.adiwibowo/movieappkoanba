//
//  Requester.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation


class Worker {
    
    static let shared: Worker = {
        let instance = Worker()
        return instance
    }()

    func getNowPlayingMovies(page: String = "1", completionHandler: @escaping (Results<NowPlayingMovies>) -> ()) {
        NetworkManager.shared.request(Router.nowPlaying(page: page), decodeToType: NowPlayingMovies.self, completionHandler: completionHandler)
    }
//    
    func searchMovies(query:String, completionHandler: @escaping (Results<SearchMovies>) -> ()) {
        NetworkManager.shared.request(Router.search(query: query), decodeToType: SearchMovies.self, completionHandler: completionHandler)
    }
//
    func movieDetail(movieId: String, completionHandler: @escaping (Results<MovieDetails>) -> ()) {
        NetworkManager.shared.request(Router.details(movieId: movieId), decodeToType: MovieDetails.self, completionHandler: completionHandler)
    }
    func creditMovie(movieId: String, completionHandler: @escaping (Results<CastModel>) -> ()) {
        NetworkManager.shared.request(Router.credits(movieId: movieId), decodeToType: CastModel.self, completionHandler: completionHandler)
    }
    func trailerMovies(movieId: String, completionHandler: @escaping (Results<TrailerEntity>) -> ()) {
        NetworkManager.shared.request(Router.trailer(movieId: movieId), decodeToType: TrailerEntity.self, completionHandler: completionHandler)
    }
}
