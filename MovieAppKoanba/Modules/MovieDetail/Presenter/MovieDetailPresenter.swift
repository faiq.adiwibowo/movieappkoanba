//
//  MovieDetailPresenter.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation


final class MovieDetailPresenter: VTPMovieDetailProtocol {
    
    var interactor: PTIMovieDetailProtocol?
    var view: PTVMovieDetailProtocol?
    var router: PTRMovieDetailProtocol?
    
    func getDetailMovie(movieId: String) {
        DispatchQueue.main.async {
            self.interactor?.getDetailMovie(movieId: movieId)
            self.interactor?.getCastMovie(movieId: movieId)
            self.interactor?.getTrailerMovie(movieId: movieId)
        }
    }
    
}

extension MovieDetailPresenter: ITPMovieDetailProtocol{
    func movieTrailerFetchedSuccessfully(trailer: TrailerEntity) {
        view?.movieTrailerFetchedSuccessfully(trailer: trailer)
    }
    
    func movieDetailFetchedSuccessfully(details: MovieDetails) {
        view?.movieDetailFetchedSuccessfully(details: details)
    }
    func movieCastFetchedSuccessfully(cast: [Cast]) {
        view?.movieCastFetchedSuccessfully(cast: cast)
    }
    func movieDetailFetchingFailed(withError: Error) {
        view?.fetchDataFailed(withError: withError)
    }
    
}
