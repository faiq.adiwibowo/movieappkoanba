//
//  MovieDetailProtocol.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation

//MARK: View -> Presenter
protocol VTPMovieDetailProtocol: AnyObject {
    var view: PTVMovieDetailProtocol? {get set}
    var router: PTRMovieDetailProtocol? {get set}
    var interactor: PTIMovieDetailProtocol? {get set}
    
    func getDetailMovie(movieId: String)
}
//MARK: Presenter -> Interactor
protocol PTIMovieDetailProtocol: AnyObject {
    var presenter:ITPMovieDetailProtocol? {get set}
    
    func getDetailMovie(movieId: String)
    func getCastMovie(movieId: String)
    func getTrailerMovie(movieId: String)
}
//MARK: Interactor -> Presenter
protocol ITPMovieDetailProtocol: AnyObject {
    
    func movieDetailFetchedSuccessfully(details: MovieDetails)
    func movieCastFetchedSuccessfully(cast: [Cast])
    func movieTrailerFetchedSuccessfully(trailer: TrailerEntity)
    
    func movieDetailFetchingFailed(withError: Error)
}
//MARK: Presenter -> Router
protocol PTRMovieDetailProtocol: AnyObject {
}
//MARK: Presenter -> View
protocol PTVMovieDetailProtocol: AnyObject{
    func movieDetailFetchedSuccessfully(details: MovieDetails)
    func movieCastFetchedSuccessfully(cast: [Cast])
    func movieTrailerFetchedSuccessfully(trailer: TrailerEntity)
    
    func fetchDataFailed(withError: Error)
}
