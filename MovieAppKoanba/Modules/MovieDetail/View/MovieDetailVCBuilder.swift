//
//  MovieDetailVCBuilder.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

class MovieDetailVCBuilder: BaseViewController {
    
    let cellId = "cellId"
    private var listGenre = [String]()
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureView()
    }
    
    private func configureView(){
        view.addSubviews(topViewContainer, movieImage, trailerView, backBtn, genreMovie, durationMovie, qualityMovie, titleMovie, trailerButton)
        view.addSubviews(botViewContainer, overviewMovie, castTitle, collectionView)
        
        topViewContainer.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(375)
        }
        movieImage.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(topViewContainer.snp.bottom)
        }
        backBtn.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(10)
            make.left.equalToSuperview().inset(10)
            make.height.width.equalTo(24)
        }
        genreMovie.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(10)
            make.bottom.equalTo(topViewContainer.snp.bottom).inset(10)
        }
        durationMovie.snp.makeConstraints { make in
            make.bottom.equalTo(genreMovie.snp.top)
            make.left.equalTo(genreMovie.snp.left)
        }
        qualityMovie.snp.makeConstraints { make in
            make.centerY.equalTo(durationMovie.snp.centerY)
            make.left.equalTo(durationMovie.snp.right).offset(5)
        }
        titleMovie.snp.makeConstraints { make in
            make.left.equalTo(genreMovie.snp.left)
            make.bottom.equalTo(durationMovie.snp.top)
        }
        botViewContainer.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(topViewContainer.snp.bottom)
        }
        overviewMovie.snp.makeConstraints { make in
            make.top.equalTo(topViewContainer.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(10)
        }
        castTitle.snp.makeConstraints { make in
            make.left.equalTo(titleMovie.snp.left)
            make.top.equalTo(overviewMovie.snp.bottom).offset(10)
        }
        collectionView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(castTitle.snp.bottom)
            make.height.equalTo(140)
        }
        trailerView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(movieImage.snp.top)
            make.bottom.equalTo(movieImage.snp.bottom)
        }
        trailerButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(trailerView.snp.centerY)
        }
        trailerView.isHidden = true
        collectionView.register(MovieDetailCell.self, forCellWithReuseIdentifier: cellId)
    }
    func setData(details: MovieDetails){
        titleMovie.text = details.title
        guard let resource = URL(string: Constants.BaseURL.imageBaseURL + (details.backdropPath ?? details.posterPath)) else {return}
        let placeholder = UIImage(named: "header")
        self.movieImage.kf.setImage(with: resource, placeholder: placeholder)
        durationMovie.text = convertToTime(minutes: details.runtime)
        for item in details.genres {
            listGenre.append(item.name)
        }
        let genreString = listGenre.joined(separator: ", ")
        genreMovie.text = genreString
        overviewMovie.text = details.overview
    }
    //MARK: - UI Elements
    private lazy var topViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(color: .grayHeader)
        return view
    }()
    private lazy var botViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    private lazy var movieImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 8
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    lazy var backBtn : UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        btn.tintColor = .black
        btn.backgroundColor = .clear
        return btn
    }()
    private lazy var titleMovie: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.kappa, .regular)
        return label
    }()
    private lazy var durationMovie: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.omega, .thin)
        return label
    }()
    private lazy var qualityMovie: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.omega, .thin)
        return label
    }()
    private lazy var genreMovie: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.omega, .thin)
        return label
    }()
    private lazy var overviewMovie: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = UIFont(.omega, .thin)
        return label
    }()
    private lazy var castTitle: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = "Cast"
        label.font = UIFont(.sigma, .regular)
        return label
    }()
    let trailerButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemRed
        button.layer.cornerRadius = 5
//        button.addBorder(color: .lightGray)
        button.setTitle("PLAY TRAILER", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        button.setTitleColor(.white, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowRadius = 2
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        return button
    }()
    let trailerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal

        //Provide Width and Height According to your need
        layout.itemSize = CGSize(width: 80, height: 124)

        //You can also provide estimated Height and Width
        layout.estimatedItemSize = CGSize(width: 80, height: 140)

        //For Setting the Spacing between cells
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20

        return UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
    }()
    
}

