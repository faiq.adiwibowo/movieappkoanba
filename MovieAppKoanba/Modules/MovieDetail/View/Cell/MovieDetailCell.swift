//
//  MovieDetailCell.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 22/04/24.
//

import Foundation
import UIKit
import SnapKit

class MovieDetailCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configureView(){
        addSubviews(actorImage, actorName)
        
        actorImage.snp.makeConstraints { make in
            make.height.width.equalTo(80)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(10)
        }
        actorName.snp.makeConstraints { make in
            make.top.equalTo(actorImage.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
        }
    }
    func configure(data: Cast){
        actorName.text = data.name.replacingOccurrences(of: " ", with: "\n")
        guard let resource = URL(string: Constants.BaseURL.imageBaseURL + (data.profilePath ?? "")) else {return}
        let placeholder = UIImage(named: "header")
        self.actorImage.kf.setImage(with: resource, placeholder: placeholder)
    }
    
    lazy var actorImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 40
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    lazy var actorName : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.font = UIFont(.omega, .thin)
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        return label
    }()
}
