//
//  MovieDetailVC.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import WebKit

class MovieDetailVC: MovieDetailVCBuilder {
    
    var presenter: VTPMovieDetailProtocol?
    var presenterViewModel = MovieDetailPresenter()
    private let disposeBag = DisposeBag()
    
    var dataDetailMovie : NowPlayingResult?
    private lazy var castData = [Cast]()
    var movieId = String()
    private var webPlayer: WKWebView!
    private var isTrailerAvailable = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.getDetailMovie(movieId: movieId)
        configureInitiateData()
        backBtn.rx.tap.subscribe(onNext: { [weak self] in
            self?.dismiss(animated: true)
        }).disposed(by: disposeBag)
        trailerButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.playTrailer()
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    private func configureInitiateData(){
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //MARK: Action to Play Trailer
        @objc func playTrailer(){
            trailerView.isHidden = false
            trailerButton.isHidden = true
            UIView.animate(withDuration: 0.2, animations: {
                       self.trailerButton.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                       self.trailerButton.alpha = 0.7
                   }) { (_) in
                       UIView.animate(withDuration: 0.2, animations: {
                           self.trailerButton.transform = CGAffineTransform.identity
                           self.trailerButton.alpha = 1.0
                       })
                   }
            !isTrailerAvailable ? self.showToast(message: "No Trailer Available", type: .warning) : nil
            
        }
}
    //  MARK: all data from called api will return here
extension MovieDetailVC: PTVMovieDetailProtocol{
    func movieTrailerFetchedSuccessfully(trailer: TrailerEntity) {
        //
        if trailer.results.isEmpty {
            isTrailerAvailable = false
        } else {
            isTrailerAvailable = true
            
            let webConfiguration = WKWebViewConfiguration()
            webConfiguration.allowsInlineMediaPlayback = true
            
            DispatchQueue.main.async {
                self.webPlayer = WKWebView(frame: self.trailerView.bounds, configuration: webConfiguration)
                self.trailerView.addSubview(self.webPlayer)
                
                guard let videoURL = URL(string: "https://www.youtube.com/embed/\(trailer.results[0].key)") else { return }
                let request = URLRequest(url: videoURL)
                self.webPlayer.load(request)
            }
        }
        
    }
    
    func movieDetailFetchedSuccessfully(details: MovieDetails) {
        setData(details: details)
    }
    func movieCastFetchedSuccessfully(cast: [Cast]) {
        self.castData = cast
        collectionView.reloadData()
    }
    
    func fetchDataFailed(withError: Error){
        self.showToast(message: "Error \(withError)", type: .danger)
    }
}

    //  MARK:   Setup CollectionView
extension MovieDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return castData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MovieDetailCell
        cell.configure(data: castData[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
