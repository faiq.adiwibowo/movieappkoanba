//
//  MovieDetailEntity.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation

// MARK: - MovieDetails
struct MovieDetails: Codable {
    let backdropPath: String?
    let id, runtime: Int
    let imdbID: String
    let overview: String
    let posterPath: String
    let releaseDate: String
    let title: String
    let video: Bool
    let voteAverage, voteCount: Double
    let genres: [GenreData]

    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case id
        case runtime
        case imdbID = "imdb_id"
        case overview
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case genres
    }
}

// MARK: - CastModel
struct CastModel: Codable {
    let crew, cast: [Cast]
    let id: Int
}

// MARK: - Cast
struct Cast: Codable {
    let id: Int
    let adult: Bool
    let knownForDepartment: String
    let profilePath: String?
    let popularity: Double
    let castID: Int?
    let originalName, creditID: String
    let order: Int?
    let character: String?
    let gender: Int
    let name: String
    let department, job: String?

    enum CodingKeys: String, CodingKey {
        case id, adult
        case knownForDepartment = "known_for_department"
        case profilePath = "profile_path"
        case popularity
        case castID = "cast_id"
        case originalName = "original_name"
        case creditID = "credit_id"
        case order, character, gender, name, department, job
    }
}
