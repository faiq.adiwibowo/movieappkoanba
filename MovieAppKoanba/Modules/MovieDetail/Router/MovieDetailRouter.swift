//
//  MovieDetailRouter.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation
import UIKit

final class MovieDetailRouter: PTRMovieDetailProtocol {
    
    var navigationController: UINavigationController?
    
    static func createMovieDetailModule() -> MovieDetailVC {
        let view = MovieDetailVC()
        
        let presenter = MovieDetailPresenter()
        let interactor : PTIMovieDetailProtocol = MovieDetailInteractor()
        let router : PTRMovieDetailProtocol = MovieDetailRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        view.presenter = presenter
        
        return view
    }
    
    func getOtherPages() {
        //
    }
}
