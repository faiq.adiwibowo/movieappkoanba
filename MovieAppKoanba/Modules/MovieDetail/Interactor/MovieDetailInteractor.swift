//
//  MovieDetailInteractor.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation
import Alamofire

final class MovieDetailInteractor: PTIMovieDetailProtocol {
    var presenter: ITPMovieDetailProtocol?
    var presenterViewModel = MovieDetailPresenter()
    
    func getHitAPI() {
        //
    }
    
    func getDetailMovie(movieId: String) {
        Worker.shared.movieDetail(movieId: movieId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                self?.presenter?.movieDetailFetchedSuccessfully(details: details)
            case .failure(let error):
                self?.presenter?.movieDetailFetchingFailed(withError: error)
            }
        }
    }
    func getCastMovie(movieId: String) {
        Worker.shared.creditMovie(movieId: movieId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let cast):
                self?.presenter?.movieCastFetchedSuccessfully(cast: cast.cast)
            case .failure(let error):
                self?.presenter?.movieDetailFetchingFailed(withError: error)
            }
        }
    }
    func getTrailerMovie(movieId: String)  {
        Worker.shared.trailerMovies(movieId: movieId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                self?.presenter?.movieTrailerFetchedSuccessfully(trailer: details)
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
