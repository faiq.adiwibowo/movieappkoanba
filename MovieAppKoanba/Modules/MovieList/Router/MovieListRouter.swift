//
//  MovieListRouter.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation
import UIKit

final class MovieListRouter: PTRMovieListProtocol {
    
    var nav: UINavigationController?
    
    static func createMovieListModule() -> MovieListVC {
        let view = MovieListVC()
        
        let presenter = MovieListPresenter()
        let interactor : PTIMovieListProtocol = MovieListInteractor()
        let router : PTRMovieListProtocol = MovieListRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        view.presenter = presenter
        
        return view
    }
    
    func getDetailPage() {
        let vc = MovieDetailRouter.createMovieDetailModule()
        nav?.pushViewController(vc, animated: true)
    }
}
