//
//  MovieListInteractor.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation
import Alamofire

final class MovieListInteractor: PTIMovieListProtocol {
    var presenter: ITPMovieListProtocol?
    var presenterViewModel = MovieListPresenter()
    
    func getHitAPI() {
        //
    }
    
    func getMovieList(page: String) {
        Worker.shared.getNowPlayingMovies(page: page) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let movies):
                UserDefaults.standard.setModelValue(value: movies.results, identifier: .movieList)
                self?.presenter?.movieListSuccess(data: movies)
            case .failure(let error):
                self?.presenter?.failedLoadApiData()
            }
        }
    }
    //  MARK:  - Search Movies
    func searchMovies(query: String) {
        Worker.shared.searchMovies(query: query) { [weak self] result in
            guard let self = self else { return }
                switch result {
                case .success(let movies):
                    self.presenter?.searchMovieSuccess(data: movies)
                case .failure(let error):
                    self.presenter?.failedLoadApiData()
            }
        }
    }
    
    //  MARK:   - Just diferrent api call style
    func getGenreList(){
        let parameters: [URLQueryItem]  = [
            URLQueryItem(name: "api_key", value: Constants.BaseURL.apiKey)
        ]
        if let url = APIConstants.getLink(baseURL: .URL_BASE, mainEndpoint: .genre, endpoints: [.movie, .list]){
            NetworkManager.shared.apiWithModel(url: url, type: GenreModel.self, method: .get, queryItem: parameters, completion: {
                decodedResponse, jsonDict in
                    DispatchQueue.main.async {
                        print(jsonDict)
                        UserDefaults.standard.setModelValue(value: decodedResponse.genres, identifier: .genre)
                    }
                
            }, errorState: {
                err in
                self.presenter?.failedLoadApiData()
            })
            
        }
    }
    
   
}
