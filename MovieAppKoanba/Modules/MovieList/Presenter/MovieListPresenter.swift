//
//  MovieListPresenter.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation

final class MovieListPresenter: VTPMovieListProtocol {
    
    var interactor: PTIMovieListProtocol?
    var view: PTVMovieListProtocol?
    var router: PTRMovieListProtocol?

    func viewDidAppear() {
        DispatchQueue.main.async {
            self.interactor?.getGenreList()
            self.interactor?.getMovieList(page: "1")
        }
    }
    func searchMovies(query: String) {
        self.interactor?.searchMovies(query: query)
    }
    func loadMore(page: String) {
        self.interactor?.getMovieList(page: page)
    }
}

extension MovieListPresenter: ITPMovieListProtocol{
    func searchMovieSuccess(data: SearchMovies) {
        view?.loadSearchedMovies(data: data)
    }
    
    func movieListSuccess(data: NowPlayingMovies) {
        view?.loadData(data: data)
    }
    
    func failedLoadApiData(){
        view?.failedLoadApiData()
    }
}
