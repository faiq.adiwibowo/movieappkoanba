//
//  MovieListProtocol.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation


//MARK: View -> Presenter
protocol VTPMovieListProtocol: AnyObject {
    var view: PTVMovieListProtocol? {get set}
    var router: PTRMovieListProtocol? {get set}
    var interactor: PTIMovieListProtocol? {get set}
    
    func viewDidAppear()
    func loadMore(page: String)
    func searchMovies(query: String)
}
//MARK: Presenter -> Interactor
protocol PTIMovieListProtocol: AnyObject {
    var presenter:ITPMovieListProtocol? {get set}
    func getMovieList(page: String)
    func getGenreList()
    func searchMovies(query: String)
    
}
//MARK: Interactor -> Presenter
protocol ITPMovieListProtocol: AnyObject {
    func movieListSuccess(data: NowPlayingMovies)
    func searchMovieSuccess(data: SearchMovies)
    func failedLoadApiData()
}
//MARK: Presenter -> Router
protocol PTRMovieListProtocol: AnyObject {
}
//MARK: Presenter -> View
protocol PTVMovieListProtocol: AnyObject{
    func loadData(data: NowPlayingMovies)
    func loadSearchedMovies(data: SearchMovies)
    func failedLoadApiData()
}
