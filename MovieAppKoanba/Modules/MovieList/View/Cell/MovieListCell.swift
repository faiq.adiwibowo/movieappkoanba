//
//  MoviewListCell.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 20/04/24.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

class MovieListCell: UITableViewCell {
    private let cellId = "cellId"
    
    private var listGenre = [String]()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: cellId)
        selectionStyle = .none
        separatorInset = .zero
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        addSubviews(movieImage, titleMovie, yearMovie, genreMovie)
        
        movieImage.snp.makeConstraints { make in
            make.height.width.equalTo(100)
            make.top.bottom.equalToSuperview().inset(5)
            make.left.equalToSuperview().inset(10)
        }
        titleMovie.snp.makeConstraints { make in
            make.top.equalTo(movieImage.snp.top).inset(5)
            make.left.equalTo(movieImage.snp.right).offset(10)
            make.right.equalToSuperview().inset(10)
        }
        yearMovie.snp.makeConstraints { make in
            make.top.equalTo(titleMovie.snp.bottom).offset(5)
            make.left.equalTo(titleMovie.snp.left)
        }
        genreMovie.snp.makeConstraints { make in
            make.left.equalTo(titleMovie.snp.left)
            make.bottom.equalTo(movieImage.snp.bottom)
        }
    }
    
    func configureCell(data: NowPlayingResult) {
        let resource = URL(string: Constants.BaseURL.imageBaseURL + (data.posterPath))
        let placeholder = UIImage(named: "header")
        self.movieImage.kf.setImage(with: resource, placeholder: placeholder)
        titleMovie.text = data.title
        yearMovie.text = "\(extractYear(from: data.releaseDate) ?? 0)"
        
        // Retrieve genres from UserDefaults
        if let genres: [GenreData] = UserDefaults.standard.getModelValue(identifier: .genre) {
            let genreIds = Set(data.genre_ids)  // Convert to set for faster lookup
            
            // Filter genres based on genre_ids
            let selectedGenres = genres.filter { genre in
                genreIds.contains(genre.id)
            }
            
            // Extract names of selected genres
            let genreNames = selectedGenres.map { $0.name }
            
            // Join genre names into a single string
            let genreString = genreNames.joined(separator: ", ")
            
            // Assign genre string to genreMovie.text
            genreMovie.text = genreString
        } else {
            print("Failed to retrieve genres")
        }
    }
    func configureCellSearch(data: SearchResult) {
        let resource = URL(string: Constants.BaseURL.imageBaseURL + (data.posterPath ?? ""))
        let placeholder = UIImage(named: "header")
        self.movieImage.kf.setImage(with: resource, placeholder: placeholder)
        titleMovie.text = data.title
        yearMovie.text = "\(extractYear(from: data.releaseDate) ?? 0)"
    }
    private func extractYear(from dateString: String) -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = dateFormatter.date(from: dateString) {
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            return year
        } else {
            print("Invalid date format")
            return nil
        }
    }
    
    //  MARK:   Component
    private lazy var movieImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 8
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.image = .strokedCheckmark
        return iv
    }()
    private lazy var titleMovie:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.omicron, .thin)
        return label
    }()
    private lazy var yearMovie:UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.numberOfLines = 1
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.omicron, .thin)
        return label
    }()
    lazy var genreMovie:UILabel = {
        let label = UILabel()
        label.textColor = UIColor(color: .grayFont)
        label.numberOfLines = 1
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont(.omega, .thin)
        return label
    }()
}
