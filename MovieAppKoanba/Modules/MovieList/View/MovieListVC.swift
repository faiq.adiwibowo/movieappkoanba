//
//  MovieListVC.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class MovieListVC: MovieListVCBuilder {
    
    var presenter: VTPMovieListProtocol?
    var presenterViewModel = MovieListPresenter()
    private let disposeBag = DisposeBag()
    private let refreshControl = UIRefreshControl()
    private var dataListMovie = [NowPlayingResult]()
    private var searchMovieArray = [SearchResult]()
    private var styleTableView = tableStyle.all
    private var loadMoreControl: LoadMoreControl!
    private var dataLoadLimit = 1
    private var isRefresh = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.dataSource = self
        tableView.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        loadMoreControl = LoadMoreControl(scrollView: tableView, spacingFromLastCell: 10, indicatorHeight: 60)
        loadMoreControl.delegate = self
        
        if let movie: [NowPlayingResult] = UserDefaults.standard.getModelValue(identifier: .movieList), !movie.isEmpty {
            setLocaleData(dataList: movie)
        } else {
            presenter?.viewDidAppear()
        }
        
        btnSearch.rx.tap
            .subscribe(onNext: { [weak self] in
                if self?.searchBar.text?.count ?? 0 > 0 {
                    guard let searchText = self?.searchBar.text else { return }
                    self?.presenter?.searchMovies(query: searchText)
                } else {
                    self?.styleTableView = .all
                    self?.tableView.reloadData()
                }
                
            })
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    private func setLocaleData(dataList: [NowPlayingResult]){
        dataListMovie = dataList
    }
    @objc func refresh(_ sender: AnyObject) {
       // Code to refresh table view
        self.isRefresh = true
        self.searchBar.text = ""
        presenter?.viewDidAppear()
    }
    
}
 
extension MovieListVC: PTVMovieListProtocol{
    func loadSearchedMovies(data: SearchMovies) {
        self.styleTableView = .search
        self.searchMovieArray = data.results
        self.tableView.reloadData()
    }
    
    func loadData(data: NowPlayingMovies) {
        self.styleTableView = .all
        
        if isRefresh {
            isRefresh = false
            dataListMovie = data.results
        } else if data.results.isEmpty {
            self.showToast(message: "No More Data Loaded", type: .warning)
        } else {
            dataListMovie.append(contentsOf: data.results)
        }
        self.tableView.reloadData()
        refreshControl.endRefreshing()
        loadMoreControl.stop()
    }
}

    //  MARK:   - TableView List Movie setup
extension MovieListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch styleTableView {
        case .all :
            return self.dataListMovie.count
        case .search :
            return self.searchMovieArray.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? MovieListCell ?? MovieListCell(style: .default, reuseIdentifier: cellId)
        
        switch styleTableView {
        case .all :
            cell.configureCell(data: self.dataListMovie[indexPath.row])
        case .search :
            cell.configureCellSearch(data: self.searchMovieArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MovieDetailRouter.createMovieDetailModule()
        vc.movieId = String(self.dataListMovie[indexPath.row].id)
        self.present(vc, animated: true)
    }
    
}
    //  MARK: - Loadmore setup
extension MovieListVC: UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        loadMoreControl.didScroll()
    }
    
}
extension MovieListVC: LoadMoreControlDelegate {
    func loadMoreControl(didStartAnimating loadMoreControl: LoadMoreControl) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dataLoadLimit = self.dataLoadLimit + 1
            self.presenter?.loadMore(page: String(self.dataLoadLimit))
        }
    }
    
    func loadMoreControl(didStopAnimating loadMoreControl: LoadMoreControl) {
        self.showToast(message: "Data Loaded", type: .success)
    }
    
    func failedLoadApiData(){
        self.showToast(message: "Failed Load Data", type: .danger)
    }
}

enum tableStyle: String{
    case search
    case all
}
