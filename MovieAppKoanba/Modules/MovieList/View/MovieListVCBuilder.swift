//
//  MovieListVCBuilder.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation
import UIKit
import SnapKit

class MovieListVCBuilder: BaseViewController {
    
    let cellId = "cellId"
    let labelAlert = UILabel()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureView()
    }
     
    private func configureView(){
        view.addSubviews(topViewContainer, btnSearch, searchBar, searchBottomLine, tableView)
        
        topViewContainer.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(100)
            make.bottom.equalTo(searchBottomLine.snp.bottom).offset(10)
        }
        btnSearch.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(-10)
            make.height.width.equalTo(25)
            make.right.equalToSuperview().inset(10)
        }
        searchBar.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.right.equalTo(btnSearch.snp.left)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(-10)
            make.height.equalTo(25)
        }
        searchBottomLine.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(15)
            make.top.equalTo(searchBar.snp.bottom).offset(10)
            make.height.equalTo(1)
        }
        
        tableView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(topViewContainer.snp.bottom)
        }
        
    }
    //MARK: - UI Elements
    lazy var topViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(color: .grayHeader)
        return view
    }()
    lazy var searchBar : UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search"
        searchBar.barStyle = .default
        searchBar.searchTextField.backgroundColor = .clear
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        searchBar.barTintColor = UIColor.clear
        searchBar.backgroundColor = UIColor.clear
        searchBar.isTranslucent = true
        
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        return searchBar
    }()
    lazy var btnSearch: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(systemName: "magnifyingglass"), for: .normal)
        btn.tintColor = .black
        return btn
    }()
    lazy var searchBottomLine : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.allowsMultipleSelection = false
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.sectionHeaderHeight = 0
        tableView.register(MovieListCell.self, forCellReuseIdentifier: cellId)
        return tableView
    }()
    
}

