//
//  UIViewController.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 22/04/24.
//

import Foundation
import UIKit

extension UIViewController {
    func showToast(_ label: UILabel, message: String, constaint: [NSLayoutConstraint]){
        
        let viewBackground = UIView()
        viewBackground.backgroundColor = .white
        viewBackground.addBorder(color: .gray)
        viewBackground.setToCurveByRadius(radius: 5)
        
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.textColor = UIColor.black
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center;
        
        label.font = UIFont.systemFont(ofSize: 15)
        //            .InterRegular(size: 12)
        label.text = message
        label.alpha = 1.0
        //        label.layer.cornerRadius = 10;
        label.clipsToBounds  =  true
        label.numberOfLines = 3
        
        self.view.addSubview(viewBackground)
        self.view.addSubview(label)
        label.isHidden = true
        viewBackground.isHidden = true
        
        NSLayoutConstraint.activate(constaint)
        NSLayoutConstraint.activate([label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                                     label.widthAnchor.constraint(lessThanOrEqualTo: self.view.widthAnchor, constant: -40),
                                     label.heightAnchor.constraint(lessThanOrEqualToConstant: 100),
                                     viewBackground.widthAnchor.constraint(equalTo: label.widthAnchor, constant: 10),
                                     viewBackground.heightAnchor.constraint(equalTo: label.heightAnchor, constant: 10),
                                     viewBackground.centerXAnchor.constraint(equalTo: label.centerXAnchor),
                                     viewBackground.centerYAnchor.constraint(equalTo: label.centerYAnchor)])
        
        viewBackground.isHidden = false
        label.isHidden = false
        
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            label.alpha = 0.0
            viewBackground.alpha = 0.0
        }, completion: {(isCompleted) in
            label.removeFromSuperview()
        })
        
    }
    
    func showToast(message: String, type: ToastType = .info) {
        let toast = Toast(frame: CGRect(x: 0, y: 0, width: view.frameWidth, height: view.frameHeight))
        toast.caption = message
        toast.toastType = type
        toast.backgroundColor = UIColor(hex: 0x000000, alpha: 0)

        view.addSubview(toast)
        toast.transform = CGAffineTransform(translationX: 0, y: view.frame.maxY)

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut) {
            toast.transform = .identity
        } completion: { _ in
            UIView.animate(withDuration: 0.15) {
                toast.backgroundColor = UIColor(hex: 0x000000, alpha: 0.2)
            }
        }

        // MARK: Autoclose 5 second
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            UIView.animate(withDuration: 0.1) {
                toast.backgroundColor = UIColor(hex: 0x000000, alpha: 0)
            }

            UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseInOut) { [unowned self] in
                toast.transform = CGAffineTransform(translationX: 0, y: self.view.frame.maxY)
            }
        }

        toast.callBackCloseBtn = { [unowned self] in
            UIView.animate(withDuration: 0.1) {
                toast.backgroundColor = UIColor(hex: 0x000000, alpha: 0)
            }

            UIView.animate(withDuration: 0.0, delay: 0, options: .curveEaseInOut) {
                toast.transform = CGAffineTransform(translationX: 0, y: self.view.frame.maxY)
            }
        }
    }
}
