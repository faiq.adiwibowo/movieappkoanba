//
//  Font.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation
import UIKit

/// Define custom font family(s) here
enum FontFamily: String {
    case poppins = "Poppins"

    static let defaultFamily = FontFamily.poppins
    var name: String { self.rawValue }
}

enum FontWeight: String {
    /// Normal
    case thin = "Regular"
    /// SemiBold
    case regular = "SemiBold"
    /// ExtraBold
    case bold = "ExtraBold"
    /// SemiBoldItalic
    case italic = "SemiBoldItalic"
    /// ExtraBoldItalic
    case italicBold = "ExtraBoldItalic"
}

enum FontSize: CGFloat {
    /// Font size 72px
    case alpha = 72.0
    /// Font size 60px
    case beta = 60.0
    /// Font size 48px
    case gamma = 48.0
    /// Font size 36px
    case delta = 36.0
    /// Font size 30px
    case epsilon = 30.0
    /// Font size 24px
    case zeta = 24.0
    /// Font size 20px
    case kappa = 20.0
    /// Font size 18px
    case lambda = 18.0
    /// Font size 16px
    case omicron = 16.0
    /// Font size 14px
    case sigma = 14.0
    /// Font size 12px
    case omega = 12.0
    /// Font size 10px
    case atom = 10.0

    /// Return font size in CGFloat
    var point: CGFloat {
        self.rawValue
    }
}

extension UIFont {
    private class func stringName(_ family: FontFamily, _ weight: FontWeight) -> String {
        let fontWeight: String

        switch (family, weight) {
        case (.poppins, .thin):
            fontWeight = FontWeight.thin.rawValue
        case (.poppins, .regular):
            fontWeight = FontWeight.regular.rawValue
        case (.poppins, .bold):
            fontWeight = FontWeight.bold.rawValue
        case (.poppins, .italic):
            fontWeight = FontWeight.italic.rawValue
        case (.poppins, .italicBold):
            fontWeight = FontWeight.italicBold.rawValue

        default:
            fontWeight = weight.rawValue
        }

        let familyName = family.name
        return fontWeight.isEmpty ? "\(familyName)" : "\(familyName)-\(fontWeight)"
    }
}

extension UIFont {

    /// Initialize UIFont with default font family
    /// - Parameters:
    ///   - size: FontSize ex: alpha, beta
    ///   - weight: FontWeight ex: regular, bold, italic, italicBold
    convenience init(_ size: FontSize, _ weight: FontWeight) {
        self.init(.defaultFamily, size, weight)
    }

    /// Initializze UIFont with font family as parameter
    /// - Parameters:
    ///   - family: FontFamily
    ///   - size: FontSize
    ///   - weight: FontWeight
    convenience init(_ family: FontFamily = .defaultFamily,
                     _ size: FontSize, _ weight: FontWeight) {
        self.init(name: UIFont.stringName(family, weight), size: size.point)!
    }

}
