//
//  UIView.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import Foundation
import UIKit

extension UIView {
    
    func addSubviews(_ views: UIView...){
        views.forEach({
            self.addSubview($0)
        })
    }
    func addBorder(color: UIColor){
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
    }
    func setToCurveByRadius(radius: CGFloat){
        self.layer.masksToBounds = false
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    var frameHeight: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y,
                                width: self.frame.size.width, height: newValue)
        }
    }

    var frameWidth: CGFloat {
        get {

            return self.frame.size.width
        }

        set {
            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y,
                                width: newValue, height: self.frame.size.height)
        }
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
