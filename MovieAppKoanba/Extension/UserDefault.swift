//
//  UserDefault.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 21/04/24.
//

import Foundation

extension UserDefaults {
    private enum Keys {
        static let GENRE_LIST = "GENRE_LIST"
        
    }
    enum ModelIdentifier: String{
        case genre
        case movieList
    }
    
    
    func setModelValue(value: Codable, identifier: ModelIdentifier){
        let encoder = JSONEncoder()
        switch identifier {
        case .genre:
            guard let value = value as? [GenreData] else {
                print("Failed to Convert Codable to Model")
                return
            }
            if let encode = try? encoder.encode(value){
                set(encode, forKey: identifier.rawValue)
            }
        case .movieList:
            guard let value = value as? [NowPlayingResult] else {
                print("Failed to Convert Codable to Model")
                return
            }
            if let encode = try? encoder.encode(value){
                set(encode, forKey: identifier.rawValue)
            }
        }
    }
    func getModelValue<T: Codable>(identifier: ModelIdentifier) -> T? {
        let decoder = JSONDecoder()
        if let data = UserDefaults.standard.data(forKey: identifier.rawValue) {
            if let decodedValue = try? decoder.decode(T.self, from: data) {
                return decodedValue
            } else {
                print("Failed to decode data for identifier: \(identifier.rawValue)")
                return nil
            }
        } else {
            print("No data found for identifier: \(identifier.rawValue)")
            return nil
        }
    }
    
}
