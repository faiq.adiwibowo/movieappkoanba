//
//  UIColor.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 20/04/24.
//

import Foundation
import UIKit

extension UIColor {
    
    /// Initialize UIColor with predefined Color
    /// - Parameter color:  Predefined color
    convenience init(color: Color) {
        self.init(named: color.rawValue)!
    }
    public convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
enum Color: String, CaseIterable {
    
    case grayFont = "grayFont"
    case grayHeader = "grayHeader"
    case grayMedium = "grayMedium"
}
