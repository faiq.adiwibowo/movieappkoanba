//
//  ViewController.swift
//  MovieAppKoanba
//
//  Created by faiq adi on 19/04/24.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigateToMovieList()
    }

    func navigateToMovieList(){
        let vc = MovieListRouter.createMovieListModule()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

